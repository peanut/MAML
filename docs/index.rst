.. MAML documentation master file, created by
   sphinx-quickstart on Sun Aug 28 16:18:31 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Mathematics and Applications of Machine Learning
================================================

Version: 2016-12-28

News:

* 2016-12-28: The handwritten number recognition challenge is open: [`Link <https://gitlab.com/deckert/MAML/blob/master/src/Handwritten%20Numbers%20Challenge/>`_]. 

Contents:

.. toctree::
    :maxdepth: 3
    :numbered:

    ./readme.rst

    ./sec-intro.rst
    ./sec-steps.rst
    ./sec-neural.rst
    ./sec-bib.rst

Possible choices of topics to proceed with:

    * Introduction into the mathematics of neural networks

        * Universal approximation theorem
        * Complexity analysis

    * Benchmark: support vector machines

        * Linear classification
        * Non-linear classification

    * Recurrent neural networks

        * Learning addition with carry
        * Pattern recognition in texts

    * Reinforcement learning

Changelog
=========

* 2016-10-27: First source code available in Section :ref:`First steps: Linear Classifiers`.
* 2016-10-26: Discussion session date and location updated.
* 2016-10-19: Online.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
